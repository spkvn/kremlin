from gremlin_call import gremlin_call
import requests


class shutdown(gremlin_call):
    def __init__(self, auth, target, argv):
        super().__init__(auth, target, argv)
        self.type = "shutdown"

    def do_post(self, params=dict()):
        r = super().do_post()
        print(r.status_code, r.reason, r.text)
