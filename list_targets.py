from gremlin_call import gremlin_call
import requests, json

class list_targets(gremlin_call):
    def __init__(self,auth, argv):
        super().__init__()
        self.api_endpoint = "/clients"
        self.auth = auth
        self.argv = argv

    def do_post(self, params=dict()):
        pass

    def do_get(self, params=dict()):
        headers = {
            "Authorization": self.auth
        }
        r = requests.get(self.url(), headers=headers).json()
        # self.api_endpoint = "/containers"
        # containers = requests.get(self.url(), headers=headers).json()
        # print(machines, containers)
        return r

    def output(self):
        targets = self.do_get()
        outstr = ""
        if len(self.argv) > 2:
            for i, target in enumerate(targets['active']):
                if target['identifier'] in self.argv:
                    outstr += json.dumps(target, indent=4, sort_keys=True)
        else:
            for i,target in enumerate(targets['active']):
                index = str(i + 1) + ") "
                outstr += index + target['identifier']
                if len(target['containers']) > 0:
                    outstr += " (" + str(len(target['containers'])) + " containers)"
                outstr += "\n"
                for j, container in enumerate(target['containers']):
                    outstr += "\t" + container['name'] + " [" + container['id'] + "](\n"
                    for k, label in container['labels'].items():
                        outstr += "\t\t" + k + ": " + label + "," + "\n"
                    outstr += "\t)\n"
        return outstr
