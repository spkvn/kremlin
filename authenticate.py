from gremlin_call import gremlin_call
import requests


class authenticate(gremlin_call):
    def __init__(self, email, password):
        super().__init__()
        self.api_endpoint = "/users/auth"
        self.email = email
        self.password = password

    def do_post(self, params=dict()):
        url = self.url()
        data = self.generate_params()
        r = requests.post(url, data=data, timeout=3)
        return r.json()[0]["header"]

    def generate_params(self):
        return {"email": self.email, "password": self.password}

    def do_get(self, params=dict()):
        pass
