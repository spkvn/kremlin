import os
from exceptions import ConfigurationFileDoesNotExistError


class configuration_reader:
    def __init__(self, path=None):
        self.path = path
        if path is None:
            self.path = os.getenv("HOME") + "/.kremlin"
        self.config = dict()
        try:
            self.file = open(self.path)
        except FileNotFoundError:
            self.write_dummy_file()
            self.file = open(self.path)

        self.parse_file()

    def parse_file(self):
        for line in self.file.readlines():
            self.parse_line(line)

    def parse_line(self, line):
        if self.line_decent_format(line) and self.line_not_ignored(line):
            key, value = line.split("=")
            value = value.rstrip("\n")
            value = value.rstrip("\r\n")
            self.config[key] = value

    def line_decent_format(self, line):
        return "=" in line

    def line_not_ignored(self, line):
        return line[0] != "#" and line[0:1] != "//"

    def get_config(self):
        return self.config

    def write_dummy_file(self):
        f = open(self.path, "w+")
        f.write("# default kremlin config file. Please update the values below\r\n")
        f.write("# gremlin api key takes precedence over the username and password\r\n")
        f.write("GREMLIN_API_KEY=\r\n")
        f.write("GREMLIN_EMAIL=\r\n")
        f.write("GREMLIN_PASSWORD=\r\n")
        f.close()
        raise ConfigurationFileDoesNotExistError
