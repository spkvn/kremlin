from attack_call import  attack_call
import requests


class cpu(attack_call):
    def __init__(self, auth, target, argv):
        super().__init__(auth, target, argv)
        self.type = "cpu"

    def do_post(self, params=dict()):
        r = super().do_post()
        print(r.status_code, r.reason, r.text)

    def do_get(self, params=dict()):
        pass
