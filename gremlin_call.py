from abc import ABC, abstractmethod


class gremlin_call(ABC):
    def __init__(self):
        self.base_url = "https://api.gremlin.com/v1"
        self.api_endpoint = "/"
        super().__init__()

    def url(self):
        return self.base_url + self.api_endpoint

    @abstractmethod
    def do_post(self, params=dict()):
        pass

    @abstractmethod
    def do_get(self, params=dict()):
        pass
