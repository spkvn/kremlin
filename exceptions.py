class ConfigurationFileDoesNotExistError(Exception):
    def __init__(self):
        self.message = "A config file doesn't exist for kremlin. One has been "\
                       "created at ~/.kremlin "
        Exception.__init__(self, self.message)


class AuthenticationException(Exception):
    def __init__(self):
        self.message = "Add either an API Key or an email / password set to " \
                       "your config file "
        Exception.__init__(self, self.message)

class ContainerNotFoundException(Exception):
    def __init__(self, identifier):
        self.message = "No container with the identifier " + identifier \
                       + " was found in active hosts"
        Exception.__init__(self, self.message)

class ContainerAttackBaseClassUsed(Exception):
    def __init__(self):
        self.message = "You have used container_attack class rather than " \
            "a subclass of it. Use a subclass to define attack type"
        Exception.__init__(self, self.message)

class AttackBaseClassUsed(Exception):
    def __init__(self):
        self.message = "You have used attack_call class rather than " \
                       "a subclass of it. Use a subclass to define attack type"
        Exception.__init__(self, self.message)