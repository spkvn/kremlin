from gremlin_call import  gremlin_call
from list_targets import list_targets
from exceptions import ContainerNotFoundException,ContainerAttackBaseClassUsed
import requests, json

class container_attack(gremlin_call):
    def __init__(self, auth, target, argv):
        super().__init__()
        self.auth = auth
        self.argv = argv
        self.api_endpoint = "/attacks/new"
        self.headers = {
            "Authorization": self.auth,
            "Content-Type": "application/json"
        }
        self.target_identifier = target
        self.type = "NO_TYPE"


    def find_target(self):
        targets = list_targets(auth=self.auth, argv=[]).do_get()
        for target in targets['active']:
            for container in target['containers']:
                if self.container_matches_target(container):
                    return {
                        "command": {"type": self.type, "args": self.argv},
                        "target": {"type": "Exact", "exact": [target['identifier']]},
                        "labels": container['labels'],
                    }
        return None

    def container_matches_target(self, container):
        return container['name'] == self.target_identifier or container['id'] == self.target_identifier

    def run(self):
        self.request_body = self.find_target()

        if self.request_body is None:
            raise ContainerNotFoundException(identifier=self.target_identifier)
        elif self.request_body['command']['type'] == "NO_TYPE":
            raise ContainerAttackBaseClassUsed
        else:
            self.do_post()

    def do_post(self, params=dict()):
        r = requests.post(self.url(), json=self.request_body, headers=self.headers)
        print(r.status_code, r.reason, r.text)

    def do_get(self, params=dict()):
        pass