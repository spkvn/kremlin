from configuration_reader import configuration_reader
from authenticate import authenticate
from list_targets import list_targets
from shutdown import shutdown
from container_shutdown import  container_shutdown
from container_cpu import container_cpu
from cpu import cpu
from sys import argv
from exceptions import AuthenticationException, ConfigurationFileDoesNotExistError, ContainerNotFoundException


class Interpreter:
    def read_conf(self):
        config = configuration_reader().get_config()
        self.email = config["GREMLIN_EMAIL"]
        self.password = config["GREMLIN_PASSWORD"]
        key = config["GREMLIN_API_KEY"]

        if key is None or key is '':
            if not ((self.email is None or self.email is '') or (self.password is None or self.password is '')):
                self.authenticate()
            else:
                raise AuthenticationException

        else:
            self.authorization = "Key " + key

    def list_targets(self, argv):
        targets = list_targets(auth=self.authorization, argv=argv).output()
        print(targets)

    def new_attack(self, argv):
        if not len(argv) > 3:
            self.attack_help()
            return
        else:
            task = argv[3]

        switcher = {
            "shutdown": shutdown,
            "cpu": cpu
        }
        # create kind of a hacky-lambda so that we can use "help" method as default.
        attack = switcher.get(task, lambda auth, target, argv: self.attack_help())
        attack_or_help = attack(auth=self.authorization, target=argv[2], argv=argv[4:])

        if attack_or_help is not None:
            attack_or_help.do_post()

    def new_container_attack(self, argv):
        if not len(argv) > 3:
            self.attack_help()
            return
        else:
            task = argv[3]

        switcher = {
            "shutdown": container_shutdown,
            "cpu": container_cpu
        }
        attack = switcher.get(task, lambda auth, target, argv: self.attack_help())
        attack_or_help = attack(auth=self.authorization, target=argv[2], argv=argv[4:])

        if attack_or_help is not None:
            attack_or_help.do_post()

    def authenticate(self, argv):
        self.authorization = authenticate(self.email, self.password).do_post()

    def attack_help(self):
        helpfile = open("./attack_help","r")
        print(helpfile.read())

    def help(self, argv):
        helpfile = open("./help","r")
        print(helpfile.read())

    def handle_task(self, task="help"):
        switcher = {
            "list-targets": self.list_targets,
            "help": self.help,
            "new-attack": self.new_attack,
            "new-container-attack": self.new_container_attack
        }
        function = switcher.get(task, self.help)
        function(argv)

    def __init__(self):
        try:
            self.read_conf()
            if len(argv) > 1:
                self.handle_task(argv[1])
            else:
                self.handle_task("help")
        except AuthenticationException as e:
            print(e.message)
            return None
        except ConfigurationFileDoesNotExistError as e:
            print(e.message)
            return None
        except ContainerNotFoundException as e:
            print(e.message)
            return None
