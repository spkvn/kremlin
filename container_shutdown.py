from container_attack import container_attack


class container_shutdown(container_attack):
    def __init__(self, auth, target, argv):
        super().__init__(auth,target,argv)
        self.type = "shutdown"
        self.run()