from gremlin_call import  gremlin_call
from exceptions import AttackBaseClassUsed
import requests

class attack_call(gremlin_call):
    def __init__(self, auth, target, argv):
        super().__init__()
        self.api_endpoint = "/attacks/new"
        self.target = target
        self.argv = argv
        self.headers = {
            'Authorization': auth,
            'Content-Type': "application/json"
        }
        self.type = "NO_TYPE"

    def generate_body(self):
        if self.type == "NO_TYPE":
            raise AttackBaseClassUsed()
        else:
            return {
                "command": {
                    "type": self.type,
                    "args": self.argv
                },
                "target": {
                    "type": "Exact",
                    "exact": [self.target]
                }
            }

    def do_post(self, params=dict()):
        r = requests.post(self.url(), headers=self.headers, json=self.generate_body())
        return r

    def do_get(self, params=dict()):
        pass